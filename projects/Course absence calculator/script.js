//stolen from Lukas Eder answer in http://stackoverflow.com/questions/4413590/javascript-get-array-of-dates-between-2-dates //
Date.prototype.addDays = function(days) {
    var dat = new Date(this.valueOf())
    dat.setDate(dat.getDate() + days);
    return dat;
}

document.onclick = calcular;

function getDates(startDate, stopDate) {
    var dateArray = new Array();
    var currentDate = startDate;
    while (currentDate <= stopDate) {
        dateArray.push( new Date (currentDate) )
        currentDate = currentDate.addDays(1);
    }
    return dateArray;
}
//end of the stole//

function calcular(){
//criar uma lista dos dias de feriados
var raw_feriados = document.getElementById("feriados").innerText;
var feriados = raw_feriados.replace(" ", "").split(",");
var new_feriados = [];
for (var i = 0; i < feriados.length; ++i){
	var feriado = feriados[i].split("-");
  var ano = feriado[0];
  var mes = feriado[1];
  var dia = feriado[2];
  new_feriados.push(new Date(ano, mes, dia));
}

//criar uma lista dos dias "letivos"
var raw_inicio = document.getElementById("data-comeco").value.replace(" ", "").split("-");
var raw_fim = document.getElementById("data-fim").value.replace(" ", "").split("-");
var inicio = new Date(raw_inicio[0], raw_inicio[1], raw_inicio[2]);
var fim = new Date(raw_fim[0], raw_fim[1], raw_fim[2]);
var intervalo_dias = getDates(inicio, fim);
var dias_letivos = [];
for (var i = 0; i < intervalo_dias.length; ++i){
	dia = intervalo_dias[i];
  var isFeriado = false;
  for (var j = 0; j < new_feriados.length; ++j){
  	if (dia == new_feriados[i]){
    	isFeriado = true;
      break;
    }
  }
  if (!isFeriado){
  	dias_letivos.push(dia);
  }
}

//responder a pergunta
var contador = [0, 0, 0, 0, 0, 0, 0];
var aulas = [document.getElementById("domingo").value, document.getElementById("segunda").value, document.getElementById("terca").value, document.getElementById("quarta").value, document.getElementById("quinta").value, document.getElementById("sexta").value, document.getElementById("sabado").value];

for (var i = 0; i < aulas.length; ++i){
	aulas[i] = parseInt(aulas[i]);
}

for (var i = 0; i < dias_letivos.length; ++i){
	d = dias_letivos[i];
  ind = d.getDay();
  contador[ind] += aulas[ind];
}

var total_aulas = 0;
for (var i = 0; i < contador.length; ++i){
	total_aulas += contador[i];
}

var texto = "Você tem um total de ";
texto += total_aulas;
texto += " aulas, o que significa que você pode faltar entre ";
texto += Math.ceil(total_aulas * 0.3) - 1;
texto += " e " + Math.ceil(total_aulas * 0.3);
texto += " vezes";

document.getElementById("resultado").innerText = texto;
return texto;
}
