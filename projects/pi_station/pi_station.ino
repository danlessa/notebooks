#include <SFE_BMP180.h>
#include <Wire.h>
#include "DHT.h"

#define BAUD_RATE 9600

#define DHTTYPE DHT22   // DHT 22  (AM2302), AM2321

#define DHTPIN 8 // Digital

#define PPD42_P1 7 // Digital
#define PPD42_P2 6 // Digital

#define BMP_SDA 4 // Analog
#define BMP_SCL 5 // Analog

SFE_BMP180 bmp;
DHT dht(DHTPIN, DHTTYPE);

unsigned long sampletime_ms = 1000;
unsigned long sample_millis = 0;

unsigned long PM10_status = 0;
unsigned long PM25_status = 0;
unsigned long PM10_count = 0;
unsigned long PM25_count = 0;
unsigned long PM_iterations = 0;
unsigned long PM_start_ms = 0;
unsigned long PM_duration = 0;

char status = 0;
unsigned long wait_until = 0;

bool BMP_temp_started = false;
bool BMP_temp_get = false;
bool BMP_pressure_started = false;

double P = 0;
double U = 0;
double T_bmp = 0;
double T_dht = 0;

bool cond1 = false;
bool cond2 = false;

void resetVars(){
  PM10_count = 0;
  PM25_count = 0;
  PM_iterations = 0;
  PM_start_ms = 0;
  PM_duration = 0;

  BMP_temp_started = false;
  BMP_temp_get = false;
  BMP_pressure_started = false;

  P = 0;
  U = 0;
  T_bmp = 0;
  T_dht = 0;

  cond1 = false;
  cond2 = false;
  sample_millis = millis() + sampletime_ms;
}


void updatePollutionVars(){
  PM10_status = digitalRead(PPD42_P1);
  PM25_status = digitalRead(PPD42_P2);

  if (PM_start_ms == 0){
    PM_start_ms = millis();
  }

  if (PM10_status == LOW){
    PM10_count += 1;
  }

  if (PM25_status == LOW){
    PM25_count += 1;
  }

  PM_iterations += 1;  
}


void setup() {
  Serial.begin(BAUD_RATE);
  pinMode(PPD42_P1, INPUT);
  pinMode(PPD42_P2, INPUT);
  
  Serial.println("@REBOOT@");
    if (bmp.begin()){
      Serial.print("[sampletime(ms):");
      Serial.print(sampletime_ms);
      Serial.print("]\n");
      Serial.print("P(hPa), Humidity(%), Temp_bmp(K), Temp_dht22(K), P10_count(#), PM25_count(#), PM_iterations(#), PM_duration(ms)\n");
      dht.begin();
    }
    else {
      Serial.println("@Error starting@");
      while(1); // Pause forever.
    }
    dht.readHumidity();
    dht.readTemperature();
    delay(sampletime_ms);
    resetVars();
}


void loop() {
  updatePollutionVars();
    if (millis() > sample_millis){
    if (BMP_temp_started == false){
      status = bmp.startTemperature();
      wait_until = millis() + status;
      BMP_temp_started = true;
    }
    
    cond1 = (millis() > wait_until);
    if (cond1 == true){
      
      if (BMP_temp_get == false){
        status = bmp.getTemperature(T_bmp);
        BMP_temp_get = true;
      }
  
      if (BMP_pressure_started == false){
        status = bmp.startPressure(3);
        wait_until = millis() + status;
        BMP_pressure_started = true;
      }
      
      cond2 = (millis() > wait_until);
      if (cond2 == true){
        status = bmp.getPressure(P, T_bmp);
        U = dht.readHumidity();
        T_dht = dht.readTemperature() + 273.15;
        T_bmp += 273.15;
        PM_duration = millis() - PM_start_ms;
        
        Serial.print(P);
        Serial.print(", ");
        Serial.print(U);
        Serial.print(", ");
        Serial.print(T_dht);
        Serial.print(", ");
        Serial.print(T_bmp);
        Serial.print(", ");
        Serial.print(PM10_count);
        Serial.print(", ");
        Serial.print(PM25_count);
        Serial.print(", ");
        Serial.print(PM_iterations);
        Serial.print(", ");
        Serial.print(PM_duration);
        Serial.print("\n");
        resetVars();
      }
    } 
  }  
}

