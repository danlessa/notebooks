#include <SFE_BMP180.h>
#include <Wire.h>
#include "DHT.h"

#define DHTPIN 13
#define DHTTYPE DHT22   // DHT 22  (AM2302), AM2321

#define BMP_SDA 1
#define BMP_SCL 0

SFE_BMP180 pressure;
DHT dht(DHTPIN, DHTTYPE);

void setup()
{
  Serial.begin(2400);
  Serial.println("@REBOOT@");

  // Initialize the sensor (it is important to get calibration values stored on the device).

  if (pressure.begin()){
    Serial.println("$P(hPa)\tU(%)\tT1(K)\tT2(K)$");
    dht.begin();
  }
  else {
    Serial.println("@Error starting@");
    while(1); // Pause forever.
  }
  dht.readHumidity();
  dht.readTemperature();
  delay(2000);
}

void loop()
{
  char status;
  double T_bmp, P, p0, a;
  double U, T_dht;
  
  // Loop here getting pressure readings every 10 seconds.

  // If you want sea-level-compensated pressure, as used in weather reports,
  // you will need to know the altitude at which your measurements are taken.
  // We're using a constant called ALTITUDE in this sketch:
  
  
  // If you want to measure altitude, and not pressure, you will instead need
  // to provide a known baseline pressure. This is shown at the end of the sketch.

  // You must first get a temperature measurement to perform a pressure reading.
  
  // Start a temperature measurement:
  // If request is successful, the number of ms to wait is returned.
  // If request is unsuccessful, 0 is returned.

  status = pressure.startTemperature();
  if (status != 0)
  {
    // Wait for the measurement to complete:
    delay(status);

    // Retrieve the completed temperature measurement:
    // Note that the measurement is stored in the variable T.
    // Function returns 1 if successful, 0 if failure.

    status = pressure.getTemperature(T_bmp);
    if (status != 0)
    {
      
      // Start a pressure measurement:
      // The parameter is the oversampling setting, from 0 to 3 (highest res, longest wait).
      // If request is successful, the number of ms to wait is returned.
      // If request is unsuccessful, 0 is returned.

      status = pressure.startPressure(3);
      if (status != 0)
      {
        // Wait for the measurement to complete:
        delay(status);

        // Retrieve the completed pressure measurement:
        // Note that the measurement is stored in the variable P.
        // Note also that the function requires the previous temperature measurement (T).
        // (If temperature is stable, you can do one temperature measurement for a number of pressure measurements.)
        // Function returns 1 if successful, 0 if failure.

        status = pressure.getPressure(P,T_bmp);
        U = dht.readHumidity();
        T_dht = dht.readTemperature();
        if (status != 0)
        {
          T_dht += 273.15;
          T_bmp += 273.15 - 0.95; //Correcting systematic error
          // Print out the measurement:
          Serial.print("#");
          Serial.print(P);
          Serial.print("\t");
          Serial.print(U);
          Serial.print("\t");
          Serial.print(T_bmp);
          Serial.print("\t");
          Serial.print(T_dht);
          Serial.print("#\n");
  
        }
        else Serial.println("@error retrieving pressure measurement@");
      }
      else Serial.println("@error starting pressure measurement@");
    }
    else Serial.println("@error retrieving temperature measurement@");
  }
  else Serial.println("@error starting temperature measurement@");

  delay(2000);  // Pause for 2 seconds.
}
