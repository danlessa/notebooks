"""
enem.py

Author: Danilo Lessa Bernardineli (danilo.lessa@gmail.com)
"""

import os
import csv
import codecs


def get_headers(path):
    """Returns ENEM headers."""
    headers = None

    with codecs.open(path, "r", encoding="iso-8859-1") as csvfile:
        csvreader = csv.reader(csvfile)
        columns = next(csvreader)
        
    return headers


def process(path, header_list):
    header_index = [headers.index(x) for x in header_list]
    header_tuples = [list(a) for a in zip(header_index, header_list)]
    
    output = {}

    for header in header_list:
        output[header] = []
    i = 0
    with codecs.open(path, "r", encoding="iso-8859-1") as csvfile:
        csvreader = csv.reader(csvfile)
        next(csvreader)
        for row in csvreader:
            for header_tuple in header_tuples:
                data = row[header_tuple[0]]
                output[header_tuple[1]].append(data)
            i += 1
            if i % 100000 == 0:
                print(i, end="\r")
    
    return output
