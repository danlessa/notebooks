import math

dt = 0.1
g = 2

arquivo_entrada = "entrada-menor.txt"

# [OK]
# devolve uma lista de listas, onde o primeiro elemento
# e uma lista de inteiros [m, n] (dimensoes da matriz)
# e os elementos subsequentes sao listas que representam
# as caracterıstica lidas dos Pokemons na forma:
# [nome, raio, x, y]
# As informa¸c~oes s~ao lidas de um arquivo "entrada.txt"
def le_arquivo():
    with open(arquivo_entrada, "r") as arquivo:
        linhas = arquivo.readlines()

    pokemons = []
    for i, linha in enumerate(linhas):
        dados = linha.split(" ")

        if (i == 0):
            # Ler tamanho da matriz
            m = int(dados[0])
            n = int(dados[1])
            pokemons.append([m, n])
        elif len(dados) >= 3:
            # Ler lista dos pokemons
            nome = dados[0]
            raio = int(dados[1])
            x = int(dados[2])
            y = int(dados[3])
            pokemons.append([nome, raio, x, y])

    return pokemons

# [OK]
# recebe o numero de linhas e de colunas da matriz
# devolve uma matriz m por n inicializada com zeros
def cria_matriz(m, n):
    matriz = []
    for i in range(m):
        linha = [0] * n
        matriz.append(linha)
    return matriz

# [OK]
# insere um Pok´emon na matriz de acordo com sua representa¸c~ao
# retangular baseada no raio ao redor do ponto central (x,y)
# id ´e o n´umero a preencher a matriz
# para o primeiro pok´emon na lista (de ´ındice zero), usa-se 1
# e assim subsequentemente
# devolve a matriz preenchida com o pok´emon dado
# primariamente, serve como fun¸c~ao auxiliar para popula_matriz() [Validar]
def preenche_pokemon(matriz, id, x, y, raio):

    # Pegar o domínio dos raios
    x_i = x - raio
    x_f = x + raio + 1

    y_i = y - raio
    y_f = y + raio + 1

    # Popular a matriz
    for i in range(x_i, x_f):
        for j in range(y_i, y_f):
            matriz[j][i] = id

    return matriz


# [OK]
# recebe uma matriz e uma lista contendo listas que representam
# os pok´emons na forma [nome, raio, x, y]
# devolve a matriz preenchida com os pok´emons conforme a
# representacao retangular considerando os raios [Validar]
def popula_matriz(matriz, pokemons):
    # Iterar na lista das listas e jogar para função de preenchimento
    for (i, pokemon) in enumerate(pokemons):
        (nome, raio, x, y) = pokemon
        matriz = preenche_pokemon(matriz, i + 1, x, y, raio)
    return matriz


# [OK]
# recebe a matriz, o numeral que representa o pok´emon a ser
# removido da matriz (id) e a lista contendo as listas que
# representam pok´emons, substituindo os numerais id por zero
# devolve a matriz com o pok´emon removido [Validar]
def remove_pokemon(matriz, id, pokemons):
    pokemon = pokemons[id - 1]
    (nome, raio, x, y) = pokemon
    matriz = preenche_pokemon(matriz, 0, x, y, raio)
    return matriz

# [OK]
# imprime a matriz dada, n~ao devolve nada
def imprime_matriz(matriz):
    for i in matriz:
        for j in i:
            if j == 0:
                print(" ", end="")
            else:
                print(j, end="")
        print()
    return None

# recebe a componente vertical vy da velocidade e devolve
# o pr´oximo valor de acordo com dt [Validar]
def atualiza_vy(vy):
    return vy + g * dt


# recebe a coordenada x e componente horizontal da velocidade atuais
# e devolve o pr´oximo valor de x [Validar]
def atualiza_x(x, vx):
    x_f = x + vx * dt
    return x_f


# recebe a coordenada y e componente vertical da velocidade atuais
# e devolve o pr´oximo valor de y [Validar]
def atualiza_y(y, vy):
    y_f = y + vy * dt
    return y_f


# converte o ^angulo theta em graus para radianos [OK]
def grau_para_radiano(theta):
    return 2 * math.pi * theta / 360

def main():

    # Obter a lista de pokemons
    leitura = le_arquivo()
    (m, n) = leitura[0]
    pokemons = leitura[1:]
    n_poke = len(pokemons)

    # Obter o número de pokebolas
    n_pokebolas = int(input("Insira a quantidade de pokébolas disponíveis: "))

    # Obter a posição inicial do treinador
    x_treinador = int(input("Insira a coordenada x do treinador: "))
    y_treinador = m - 1

    # Criar a matriz
    matriz = cria_matriz(m, n)
    matriz = popula_matriz(matriz, pokemons)

    # Colocar a posição inicial do treinador
    matriz[y_treinador][x_treinador] = "T"

    # Iterar enquanto houver pokebolas
    for iteracao in range(n_pokebolas):

        # Checar se ainda tem pokemons restantes para continuar
        if n_poke == 0:
            break

        # Imprimir o número de pokebolas
        print("\n{} pokébolas disponíveis".format(n_pokebolas - iteracao))

        # Imprimir o estado atual da matriz
        print("Estado atual do mundo com Pokémons:")
        imprime_matriz(matriz)

        # Pegar parametros de lançamento
        v_lancamento = float(input("Insira a velocidade de lançamento: "))
        angulo = float(input("Insira o ângulo de lançamento: "))

        angulo_rad = grau_para_radiano(angulo)
        x = x_treinador
        y = y_treinador
        vx = v_lancamento * math.cos(angulo_rad)
        vy = -v_lancamento * math.sin(angulo_rad)

        # Gerar uma matriz para visualizaçao da pokebola
        matriz_lancamento = cria_matriz(m, n)
        matriz_lancamento = popula_matriz(matriz_lancamento, pokemons)
        matriz_lancamento[y_treinador][x_treinador] = "T"

        while True:
            pegou_pokemon = False

            # Simular a posicao da pokebola
            x = atualiza_x(x, vx)
            x_d = round(x)
            y = atualiza_y(y, vy)
            y_d = round(y)
            vy = atualiza_vy(vy)

            # Condicoes para quebrar a simulacao
            condicoes = (x >= 0)
            condicoes &= (x < n)
            condicoes &= (y < m)
            condicoes &= (pegou_pokemon == False)
            if condicoes is False:
                break

            # Condicao para pokebola dentro da matriz
            if (y_d >= 0):
                print((y_d, x_d))
                matriz_lancamento[y_d][x_d] = "."

                # Checar se pegou pokemon
                if(matriz[y_d][x_d] != 0):
                    pegou_pokemon = True
                    matriz_lancamento[y_d][x_d] = "X"
                    id = matriz[y_d][x_d]
                    nome = pokemons[id - 1][0]
                    break

        print("Representação gráfica aproximada do lançamento efetuado: {}".format(i_lance))
        imprime_matriz(matriz_lancamento)

        # Se capturar pokemon:
        matriz[y_treinador][x_treinador] = 0
        if pegou_pokemon is True:
            nome = pokemons[id - 1][0]
            print("Um {} foi capturado!".format(nome))
            matriz[y_treinador][x_treinador] = 0
            matriz = remove_pokemon(matriz, id, pokemons)
            x_treinador = x_d
            matriz[y_treinador][x_treinador] = "T"
            n_poke -= 1
        else:
            print("O lançamento não capturou Pokémon algum")
            x_treinador = int(input("Insira a coordenada x do treinador: "))
        matriz[y_treinador][x_treinador] = "T"

    if n_poke == 0:
        print("Parabens! Todos pokemons disponiveis foram capturados")
    else:
        print("Faltaram {} pokemons :(".format(n_poke))

    # Motivo de encerramento do programa
    return None

if __name__ == "__main__":
    main()
